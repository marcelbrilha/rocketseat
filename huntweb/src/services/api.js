import { create } from 'axios';

const api = create({
  baseURL: 'https://rocketseat-node.herokuapp.com/api'
});

export default api;
