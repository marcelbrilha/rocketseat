// EXAMPLE
// const INITIAL_STATE = {
//   data: [],
//   loading: false,
//   error: false
// }

const todos = (state = [], action) => {
  switch (action.type) {
    case 'ADD_TODO':
      return [...state, {
        id: Math.random(),
        text: action.payload.text
      }];
    default:
      return state;
  }
}

export default todos;
