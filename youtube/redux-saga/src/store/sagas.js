import { takeLatest, put, call, select, all } from 'redux-saga/effects';

const apiGet = (text, length) => (
  new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve(`${text} da RocketSeat - ${length}`);
    }, 2000)
  })
);

function* asyncAddTodo (action) {
  try {
    const todos = yield select(state => state.todos); // GET State
    const response = yield call(apiGet, action.payload.text, todos.length);
    yield put({ type: 'ADD_TODO', payload: { text: response }})
  } catch (error) {
    console.log(`Emit error: ${error}`);
    // TODO yield put({ type: 'ERROR' });
  }
}

export default function* root() {
  return yield all([
    // takeEvery('ASYNC_ADD_TODO', asyncAddTodo),
    takeLatest('ASYNC_ADD_TODO', asyncAddTodo)
  ])
}
